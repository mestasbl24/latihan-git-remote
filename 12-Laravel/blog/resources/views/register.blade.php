<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="home.html">
        <label for="1">First name:</label> <br><br>
        <input type="text" placeholder="inputFirstName"> <br><br>
        <label for="2">Last name:</label> <br><br>
        <input type="text" placeholder="inputLastName"> <br><br>
        <label for="gender">Gender:</label> <br><br>
        <input type="radio" name="jenis_kelamin" value="Laki-laki" >Male <br>
        <input type="radio" name="jenis_kelamin" value="Perempuan" >Female <br>
        <input type="radio" name="jenis_kelamin" value="lainnya" >Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="kebangsaan" >
            <option value="indonesia">Indonesian</option>
            <option value="malaysia">Malaysia</option>
            <option value="america">America</option>
        </select> <br><br>
        <label for="bahasa">Language Spoken:</label> <br><br>
        <input type="checkbox" name="indonesia" >
        <label for="ind">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="inggris">
        <label for="ing">English</label> <br>
        <input type="checkbox" name="lainnya" >
        <label for="oth">Other</label> <br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="biodata" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up" > 


    </form>
</body>
</html>
